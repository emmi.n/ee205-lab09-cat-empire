///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   01_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class Cat {
private:    /// Member variables
	std::string name;

protected:
   Cat* left  = nullptr;
   Cat* right = nullptr;
   int depth  = 0; // @note keeps track of a cat's depth, root cat's depth is 1

public:     /// Constructors
	Cat( const std::string newName );
	friend class CatEmpire;

private:    /// Private methods
	void setName( const std::string newName ); // Name is a key in our BST, so we don't
		                                        // want to let people change it as it'll
		                                        // mess up the tree.

private:    /// Static variables
	static std::vector<std::string> names;

public:     /// Static methods
	static void initNames();
	static Cat* makeCat();
};


class CatEmpire {
private:
	Cat* topCat = nullptr;
   unsigned int nodeCount = 0;   // internal count of the # of nodes aka cats in BT
public:
	static constexpr const int   nameLen = 6;  /// Constant

public:
	bool empty() const;                             // Return true if empty
	inline unsigned int size() const;               // Returns number of nodes in tree
	bool isIn( Cat* aCat, Cat* atCat ) const;       // Return true if aCat is anywhere below atCat in the BST
	inline Cat* getTopCat() const;                  // Return topCat 
	void printName( Cat* aCat ) const;              // Prints out aCat's name
	
   const bool validate() const;                    // Return true if BT is valid
	const bool validateCat( Cat* aCat ) const;      // Return true if aCat is valid

public:
	void addCat( Cat* newCat );  // Add a cat starting at the root

	void catFamilyTree() const;
	void catList() const;
	void catBegat() const;
	void catGenerations() const;

private:
	void addCat( Cat* atCat, Cat* newCat );  // Add a cat starting at atCat

	void dfsInorderReverse( Cat* atCat, int depth ) const;
	void dfsInorder( Cat* atCat ) const;
	void dfsPreorder( Cat* atCat ) const;
	void bfs() const;

   void getEnglishSuffix( int n ) const;  // a utility method for bfs(); prints proper english suffix
};

inline unsigned int CatEmpire::size() const { return nodeCount; }

inline Cat* CatEmpire::getTopCat() const { return topCat; }

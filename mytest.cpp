///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file mytest.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   01_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <iostream>

#include "cat.hpp"

using namespace std;

int main(){
   cout << "Welcome to my test" << endl;

   Cat::initNames();
   CatEmpire purrsianEmpire;
   Cat* newCat = nullptr;
   
   cout << "Is it empty: " << boolalpha << purrsianEmpire.empty() << endl;
   cout << "Is it valid: " << boolalpha << purrsianEmpire.validate() << endl;
   cout << "Number of Nodes: " << purrsianEmpire.size() << endl;
  
   for( int i = 0; i < 100; i++){
      newCat = Cat::makeCat();
      purrsianEmpire.addCat( newCat );

      assert( !purrsianEmpire.empty() );
      assert( purrsianEmpire.validate() );
      purrsianEmpire.validateCat( newCat );
      //cout << "Number of Nodes: " << purrsianEmpire.size() << endl;
   }
   cout << "Number of Nodes: " << purrsianEmpire.size() << endl;

   CatEmpire ronyanEmpire;

   cout << "--------------------NEW EMPIRE---------------------" << endl;
   cout << "Is it empty: " << boolalpha << ronyanEmpire.empty() << endl;
   cout << "Is it valid: " << boolalpha << ronyanEmpire.validate() << endl;
   cout << "Number of Nodes: " << ronyanEmpire.size() << endl;
   cout << "___Printing a family tree of 0 cats___"<< endl;
   ronyanEmpire.catFamilyTree();
   cout << "list of new cats added: " << endl;

   newCat = nullptr;
   for( int i = 0; i < 12; i++){
      newCat = Cat::makeCat();
      ronyanEmpire.validateCat( newCat );
      
      cout << "Is in: ";
      cout << boolalpha << ronyanEmpire.isIn( newCat, ronyanEmpire.getTopCat() ) << endl;

      ronyanEmpire.printName( newCat );
      ronyanEmpire.addCat( newCat );
      
      cout << "Is in: ";
      cout << boolalpha << ronyanEmpire.isIn( newCat, ronyanEmpire.getTopCat() ) << endl;

      assert( !ronyanEmpire.empty() );
      assert( ronyanEmpire.validate() );
   }
   cout << "_______________________________________"<< endl;
   cout << "___Printing a family tree of 12 cats___"<< endl;
   cout << "_______________________________________"<< endl;
   ronyanEmpire.catFamilyTree();

   cout << "_______________________________________"<< endl;
   cout << "_Print an alphabetized list of 12 cats_" << endl;
   cout << "_______________________________________"<< endl;
	ronyanEmpire.catList();
   
   cout << "_______________________________________"<< endl;
   cout << "______Print a pedigree of 12 cats______" << endl;
   cout << "_______________________________________"<< endl;
	ronyanEmpire.catBegat();

   cout << "_______________________________________"<< endl;
   cout << "___Print the Generations of  12 cats___" << endl;
   cout << "_______________________________________"<< endl;
	ronyanEmpire.catGenerations();

   // check if names is compared by length or alphabetical order
   //cout << boolalpha << ("Shirley" < "Stimpy") << endl;

   cout << "This is the end of the test." << endl;
   return 0;
}

###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author EmilyPham <emilyn3@hawaii.edu>
# @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
# @date   22_APR_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

AR       = ar

TARGETS  = familyTree catList catBegat catGenerations mytest

all: $(TARGETS)

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

validate.o: validate.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

libcat.a: cat.o validate.o
	$(AR) -rsv $@ $^

$(TARGETS): %: %.cpp libcat.a			# this line acts like a for loop going through all targets
	$(CXX) $(CXXFLAGS) -o $@ -L. $< -lcat

clean:
	rm -f *.o *.a $(TARGETS)

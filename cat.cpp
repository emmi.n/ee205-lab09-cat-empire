///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   04_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

//#define DEBUG

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

// start of CatEmpire methods
bool CatEmpire::empty() const {
#ifdef DEBUG
   validate();
#endif 
   return ( (topCat == nullptr) );
}

bool CatEmpire::isIn( Cat* aCat, Cat* atCat ) const {
#ifdef DEBUG
   validate();
#endif
   if( atCat == aCat )         { return true; }   // first base case,  aCat is in the BST
   else if( atCat == nullptr ) { return false; }  // second base case, reached the bottom of the BST
 
#ifdef DEBUG
   validateCat( aCat );
   validateCat( atCat );
#endif
   bool returnValue;
   returnValue = isIn( aCat, atCat->left );        // first search the left
   if( returnValue == true ) { return true; }      // aCat is in list, so undwind and reutrn true
   
   returnValue = isIn( aCat, atCat->right );       // otherwise search the right
   if( returnValue == true ) { return true; }      // aCat is in list, so undwind and reutrn true
   else                      { return false; }     // otherwise atCat is not in the list  
}

void CatEmpire::printName( Cat* aCat ) const {
#ifdef DEBUG
   assert( validateCat( aCat) );
#endif
   cout << aCat->name << endl;
}

void CatEmpire::addCat( Cat* newCat ) {  // Add a cat starting at the root
#ifdef DEBUG
   validate();
   assert( validateCat( newCat ) );
#endif
   assert( newCat != nullptr );
   assert( !isIn( newCat, topCat) );

   if( topCat == nullptr ) {
      topCat = newCat;
      newCat->depth = 1;         // set depth to 1
   } else {
      addCat( topCat, newCat );  // call the internal addCat()
   }

   nodeCount++;                  // update internal count
#ifdef DEBUG
   validate();
#endif
   return;
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {  // Add a cat starting at atCat
#ifdef DEBUG
   validate();
   assert( validateCat( atCat ) );
   assert( validateCat( newCat ) );
#endif
   assert( atCat  != nullptr );
   assert( newCat != nullptr );
  
   if( atCat->name > newCat->name ) {  // atCat is greater so newCat should be to the left of it
      if( atCat->left == nullptr ) {
         atCat->left = newCat;
         newCat->depth = atCat->depth + 1; 
      } else {
         addCat( atCat->left, newCat );
      }
   }

   if( atCat->name < newCat->name ) {  // atCat is smaller so newCat should be to the right of it
      if( atCat->right == nullptr ) {
         atCat->right = newCat;
         newCat->depth = atCat->depth + 1; 
      } else {
         addCat( atCat->right, newCat );
      }
   }
   return;
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorderReverse( topCat, 1 );
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorder( topCat );
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}

void CatEmpire::catGenerations() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   bfs();
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
#ifdef DEBUG
   validate();
#endif
   if( atCat == nullptr ) { return; }              // at the bottom of BT, base case

#ifdef DEBUG
   assert( validateCat( atCat ) );
#endif
  
   dfsInorderReverse( atCat->right, (depth+1) );   // first go to the right, also increment depth
   
   // print formatted name, start of unwinding code
   cout << string( nameLen * (depth-1), ' ' ) << atCat->name;
   if( (atCat-> right == nullptr) && (atCat-> left == nullptr) ) {         //it's a leaf node
      cout << endl;
   } else if( (atCat-> right != nullptr) && (atCat-> left != nullptr) ) {  // it has both a left and right child
      cout << "<" << endl;
   } else if( (atCat-> right == nullptr) && (atCat-> left != nullptr) ) {  // it only has a left child
      cout << "\\" << endl;
   } else if( (atCat-> right != nullptr) && (atCat-> left == nullptr) ) {  // it only has a right child
      cout << '/' << endl;
   }
   
   dfsInorderReverse( atCat->left, (depth+1) );    // now go to the left, also increment depth
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
#ifdef DEBUG
   validate();
#endif
   if( atCat == nullptr ) { return; }        // at the bottom of BT, base case
 
#ifdef DEBUG
   assert( validateCat( atCat ) );
#endif
  
   dfsInorder( atCat->left );                // first go to the left
   cout << atCat->name << endl;              // print name, start of unwinding code
   dfsInorder( atCat->right );               // now go to the right
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
#ifdef DEBUG
   validate();
#endif
   if( atCat == nullptr ) { return; }        // at the bottom of BT, base case
 
#ifdef DEBUG
   assert( validateCat( atCat ) );
#endif
  
   // print formatted name
   if( (atCat-> right != nullptr) && (atCat-> left != nullptr) ) {         // it has both a left and right child
      cout << atCat->name << " begat " << atCat->left->name << " and " <<  atCat->right->name << endl;
   } else if( (atCat-> right == nullptr) && (atCat-> left != nullptr) ) {  // it only has a left child
      cout << atCat->name << " begat " << atCat->left->name << endl;
   } else if( (atCat-> right != nullptr) && (atCat-> left == nullptr) ) {  // it only has a right child
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }  // else it has no children, so print nothing

   dfsPreorder( atCat->left );            // first go to the left
   dfsPreorder( atCat->right );           // now go to the right
}

void CatEmpire::bfs() const {
#ifdef DEBUG
   validate();
#endif
   if( topCat == nullptr ) { return; };         //base case
   
   queue<Cat*> catQueue;                        // queue of cats that have been discovered
   Cat* frontCat = nullptr;
   Cat* prevCat = nullptr;                      // keeps track of previous cat

   catQueue.push( topCat );                     // enqueue topCat

   while( !catQueue.empty() ){ 
      frontCat =  catQueue.front();             // dequeues a cat
      catQueue.pop();

      if( frontCat == nullptr ) { return; }     // something is wrong so return
      
      if( frontCat == topCat && prevCat == nullptr ){ // it's the first generation, print out header
         cout << frontCat->depth;
         getEnglishSuffix( frontCat->depth );
         cout << " Generation" << endl;
      }else if( frontCat->depth > prevCat->depth ){   // it's a new generation! print out new header
         cout << endl << frontCat->depth;
         getEnglishSuffix( frontCat->depth );
         cout << " Generation" << endl;
      }
      cout << "  " << frontCat->name;           // print cat's name

      if( frontCat->left != nullptr ){          // add frontCat's children to the queue
         catQueue.push( frontCat->left );
      }
      if( frontCat->right != nullptr ){
         catQueue.push( frontCat->right );
      }

      prevCat = frontCat;                       // the front cat is now the previous cat
   } // end while
   cout << endl;
}

void CatEmpire::getEnglishSuffix( int n ) const {
   assert( n > 0 );

   if( (10 <= n) && (n <= 19) ) {         // if tens digit is a 1, suffix is th
      cout << "th";
      return;
   }

   switch( n % 10 ){                      // otherwise, if units digit is a 1, 2, or 3 it is a special case
      case 1: 
         cout << "st";
         break;
      case 2: 
         cout << "nd";
         break;
      case 3: 
         cout << "rd";
         break;
      default:
         cout << "th";
   } // end switch
}

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file validate.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   01_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>
#include <string>
#include <map>

#include "cat.hpp"

using namespace std;

// @note unfortunately I didn't have time to do a full validation of my BST
// @note but I did do a surface level validation

const bool CatEmpire::validate() const {
   // @note validation of emptiness:
   // @note if found invalid sytem will stop process and throw flag
   if( topCat == nullptr ){
      assert( nodeCount == 0 );
   } else {
      assert( topCat != nullptr );
      assert( nodeCount != 0 );
   }

   return true; // if it passed all the tests it's valid!
}

const bool CatEmpire::validateCat( Cat* aCat ) const {
   assert( !aCat->name.empty() );   // all cats MUST have a name
   return true; //if it passed all the tests it's valid!
}
